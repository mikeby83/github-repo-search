// get the form
const gitHubSearch = document.getElementById('gitHubSearch');
let ul = document.getElementById('userRepos');

// submit button listener
gitHubSearch.addEventListener('keyup', (e) => {
  e.preventDefault();
  //clear results on each keystroke
  ul.innerHTML = '';
  // get the form field
  let usernameInput = document.getElementById('usernameInput');

  // put the field value into var
  const gitHubUsername = usernameInput.value;

  // run github api - below
  requestUserRepos(gitHubUsername);

})


function requestUserRepos(username) {


  const xmlrequest = new XMLHttpRequest();

  // use what's typed to get a github username, and then order their repos by star value
  const url = `https://api.github.com/users/${username}/repos?per_page=5&sort=created&direction=desc`;

  // Open a new connection
  xmlrequest.open('GET', url, true);



  xmlrequest.onload = function() {

    // Parse API data into JSON
    const data = JSON.parse(this.response);

    // Loop over each object in data array
    for (let i in data) {

      // Get the ul with id of of userRepos
      let intro = document.getElementById('userInfo');
      intro.innerHTML = (`
              <div class="card">
                <div class="card-header">
                <img src="https://avatars.githubusercontent.com/u/${data[i].owner.id}"/>
                   <h4 class="d-inline mb-0">${data[i].owner.login}</h4>
                 </div>
              </div>
              `)



      // Create variable that will create li's to be added to ul
      let li = document.createElement('li');

      // Add Bootstrap list item class to each li
      li.classList.add('list-group-item')

      //short delay, then add load class for fancy animation
      setTimeout(function() {
        li.className += " load";
      }, 200);

      // Create the html markup for each li
      li.innerHTML = (`
                <p class="created_at float-end">${data[i].created_at}</p>
                <h5>${data[i].name}</h5>
                <span class="badge bg-primary">${data[i].forks} Forks</span>
                <span class="badge bg-secondary">${data[i].watchers} Watchers</span>
                <span class="badge bg-success">${data[i].stargazers_count} Stars</span>
                <p class="description">${data[i].description}</p>
                <a class="btn btn-outline-primary btn-sm" href="${data[i].html_url}/commits" target="_blank">View Commits</a>
            `);

      // Append each li to the ul
      gitHubSearch.addEventListener('keyup', (e) => {

        if (usernameInput.value.length == 0) {
          intro.innerHTML = ('');
          console.log("emoty");
        }
      });

      //update the list items
      ul.appendChild(li);

    }

  }


  // Send the request to the server
  xmlrequest.send();

}
